##  Acceptance tests

Ensure system meets requirements via set of test cases.

Crosses components and to validate whole user stories from a purely user centric view, without manipulating the system below.

<br>
### Benefits

1. Ensure business logic is filled via user stories
2. Ensure requirements are fulfilled

<br>
Written by **QA team**.

_"Developers should not test their own code"_ -applies here.
