##  Unit tests

Test **individual** units of source code, like single methods, views or components. 
<br><br>
### Benefits

1. Find problems early
2. Facilitates change
3. Documentation
4. Design

> <small>(source: http://en.wikipedia.org/wiki/Unit_testing#Benefits)</small>

Written by **developers**.

<aside class="notes">
    <ol>
    <li>Unit testing finds problems early in the development circle. Later in the development cycle, unit tests safe guard against nasty regression.</li>
    <li>Unit tests allow for safer refactoring. Everything doesn't need to be manually checked during a refactoring process if there is good test coverage.</li>
    <li>Unit tests provide a sort of documentation of the system. Developers can use existing unit tests to understand critical functionality of the system.</li>
    <li>Unit tests help with design. While writing tests one has to think about the practical implementation of the code.</li>
    </ol>
</aside>
