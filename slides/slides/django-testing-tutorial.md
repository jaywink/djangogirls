### Django testing tutorial

Explains not only why to test, but also walks trough testing in Django using the Django tutorial poll app.

https://docs.djangoproject.com/en/1.7/intro/tutorial05/

<small>_... could have just used that tutorial here, but oh well, this is for PyLadies... :)_</small>
