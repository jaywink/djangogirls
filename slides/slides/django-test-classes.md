### Django test classes

![](https://docs.djangoproject.com/en/1.7/_images/django_unittest_classes_hierarchy.svg)
<small>_source: [Django testing documentation](https://docs.djangoproject.com/en/1.7/topics/testing/tools/#provided-test-case-classes)_</small>

Consider subclassing one of these (probably basic TestCase) and extend it to your own project needs.

