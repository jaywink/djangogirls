### TDD example - implement!

Let's implement our feature, as per the [DjangoGirls extensions book](http://djangogirls.gitbooks.io/django-girls-tutorial-extensions/content/homework/README.html#add-publish-button).

<small>_Note, we're skipping the draft page, we will just add the button. We've already placed drafts in the main list.._</small>

