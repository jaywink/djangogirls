### Increase test coverage over time
<br>

Lots of code and no tests getting you down?

Don't worry. Instead, when you fix or change something, or write a new feature - **write a test for it**.

Your code coverage will increasy organically.

Don't try to _reserve time_ to implement code coverage - that time will never become available!
