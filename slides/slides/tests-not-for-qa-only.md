##  Tests are not for QA<br> people only

<br>

### Different types of testing

* Unit tests
* Integration tests
* Acceptance tests

<br>

<small>Disclaimer: Testing is a large subject - this is only one view into it.</small>
