## It looks good at the start

<br>

    >>> will_the_snow_melt_today('-5')
    >>> "Nope! It's here to stay."
    
    >>> will_the_snow_melt_today('20')
    >>> "It probably will."

    >>> will_the_snow_melt_today('0')
    >>> "I don't know what that means!"
    
<br>

## ... but what's this?