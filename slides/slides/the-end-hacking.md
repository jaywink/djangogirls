## Some things to hack on

### Add view count to blog post
  * add to model a count
  * add method to increment count
  * add call to method from view view
  * tests
    - test counter increments when viewing view

### Add commenting to posts
  * comment model with two fields - name, text
  * add view save comment action
  * add model 'add comment' action
  * tests
    - test template has input fields
    - test view for comment saving functionality
    - test comment saving on model side
