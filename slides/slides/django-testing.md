##  So which one are we talking about here?

Testing in relating to what a developer does when writing code, we are talking primarily about **unit testing**.

The Django and unittest framework can be used for integration testing too.

For acceptance testing, other tools are available, for example browser automation via Selenium IDE, which works purely in the user level.
