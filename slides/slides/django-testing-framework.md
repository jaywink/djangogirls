### Django testing framework

Based on Python [Unittest framework](https://docs.python.org/3/library/unittest.html).

Comprehensive documentation [online](https://docs.djangoproject.com/en/1.7/topics/testing/).
