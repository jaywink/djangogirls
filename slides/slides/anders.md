<a href="http://andersinno.fi">
    <img style="background-color: white; border: 0 none transparent; padding: 0; width: 1280px;" src="https://www.andersinnovations.com/static/bootstrap/img/logo-anders-fi.svg">
</a>

We do e-commerce. [Join us!](https://www.andersinnovations.com/en/company/careers/)

<img style="border: 0 none transparent; padding: 0; float: left; width: 50%;" src="https://www.andersinnovations.com/static/bootstrap/img/logot/png/shoop.png">

<div style="float: right; width:50%; padding-top:30px;">
    Open source e-commerce platform - coming in 2015!
</div>
