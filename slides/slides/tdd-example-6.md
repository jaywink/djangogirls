### TDD example - run tests again

    $ python manage.py test blog.tests.SuperUserTestCase
    Creating test database for alias 'default'...
    ......
    ----------------------------------------------------------------------
    Ran 6 tests in 0.510s

    OK
    Destroying test database for alias 'default'...

No more failures!
