### TDD example - why?

* Writing tests before writing code improves design. Creating the test is like writing out the specifications. You will more likely spot problems in for example business logic *before wasting time writing code*.
