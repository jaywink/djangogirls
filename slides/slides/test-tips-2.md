### Follow TDD when possible
<br>

**TDD (Test Driven Development)** basically means **write failing test, then code to pass the test**.

Not always possible (or sensible) when creating features, but **always** sensible when fixing bugs.
