## Sample output
<br>
<pre>
    Traceback (most recent call last):
        File "foo.py", line 44, in test_snow_does_not_melt_below_three_degrees
            self.assertEqual("Nope! It's here to stay.", answer)
    AssertionError: "Nope! It's here to stay." != "I don't know what that means!"
    - Nope! It's here to stay.
    + I don't know what that means!
</pre>