##  Integration tests

Groups together several project components according to a test plan.

<br>
### Benefits

1. Find problems with different system components working together.
2. Find breakage with business logic across system

<br>
Written by **developers or QA team**.
