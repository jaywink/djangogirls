### Install for development

From: https://github.com/slara/generator-reveal

Node.js needed.

```
sudo npm install -g grunt-cli
sudo npm install -g generator-reveal
sudo npm install -g bower
```

### Running server

```
cd slides
npm install
bower install
grunt server
```

_Maybe not all these needed, but works after these at least... :)_

### Adding slides

In `slides/`, see https://github.com/slara/generator-reveal commands.